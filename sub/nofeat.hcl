version = "1.0"

train {
    image = "tensorflow/tensorflow:1.13.1-py3"
    install = ["pip install -q pyyaml"]
    script = ["python3 train.py"]

    parameters {}
}

serve {
    image = "mhart/alpine-node:11.10.1"
    install = [
        "yarn global add http-server"
        ]
    script = ["http-server /artefacts"]
}
