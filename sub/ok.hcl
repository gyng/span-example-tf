version = "1.0"

train {
    image = "python:3.7"
    install = []
    script = ["pwd && ls -la"]

    parameters {
        SUBPROJECT_TRAIN = "true"
    }

    secrets = [
        "A",
        "B"
    ]
}

serve {
    image = "python:3.7"
    install = ["echo"]
    script = ["cd /model-server && ls -la && python3 -m http.server $SERVER_PORT"]
}

features {
    image = "python:3.7"
    install = []
    script = ["pwd && ls -la"]

    parameters {
        SUBPROJECT_FEATURE = "true"
    }

    secrets = [
    ]

    feature_definition {
      name = "a"
      key = "a"
      description = "a"
    }

    feature_definition {
      name = "b"
      key = "b"
      description = "b"
    }
}
