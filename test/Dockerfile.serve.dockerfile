FROM mhart/alpine-node:11.10.1

EXPOSE 3000

RUN ["yarn", "global", "add", "http-server"]

COPY test/artefacts /artefacts

RUN ["http-server", "-a", "0.0.0.0", "-p", "3000", "/artefacts"]
